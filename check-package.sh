#!/bin/bash

# Array of packages to install
my_array=("git" "vim" "tldr")

# Check for package manager
if command -v apt-get &> /dev/null
then
    # Update package manager
    sudo apt-get update
    
    # Install packages
    sudo apt-get install "${my_array[@]}"
    
elif command -v yum &> /dev/null
then
    # Update package manager
    sudo yum update
    
    # Install packages
    sudo yum install "${my_array[@]}"
    
elif command -v pacman &> /dev/null
then
    # Update package manager
    sudo pacman -Sy
    
    # Install packages
    sudo pacman -S "${my_array[@]}"
    
else
    echo "No package manager found."
    exit 1
fi

